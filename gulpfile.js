const child = require('child_process');
const browserSync = require('browser-sync').create();

const gulp = require('gulp');
const concat = require('gulp-concat');
const gutil = require('gulp-util');
const sass = require('gulp-sass');

const siteRoot = '_site';
const cssFiles = '_css/**/*.?(s)css';

//var spawn = require('child_process').spawn;

gulp.task('css', () => {
  gulp.src(cssFiles)
    .pipe(sass())
    .pipe(concat('all.css'))
    .pipe(gulp.dest('assets'));
});

gulp.task('jekyll-build', () => {
  const jekyll = child.spawn('bundle', ['exec', 'jekyll', 'build',
    '--watch',
    '--incremental',
    '--drafts',
    '--destination=../' + siteRoot
  ],{ cwd: 'jekyll/'});

  const jekyllLogger = (buffer) => {
    buffer.toString()
      .split(/\n/)
      .forEach((message) => gutil.log('Jekyll: ' + message));
  };

  jekyll.stdout.on('data', jekyllLogger);
});


//gulp.task('jekyll-node-process', function(done) {
//  spawn('bundle', ['exec', 'jekyll', 'build', '--watch','--incremental'], { cwd: 'jekyll/', stdio: 'inherit' })
//  .on('close', done);
//});

gulp.task('serve', () => {
  browserSync.init({
    files: [siteRoot + '/**'],
    port: 4000,
    server: {
      baseDir: siteRoot
    },
    open: false
  });

  gulp.watch(cssFiles, ['css']);
});

gulp.task('default', ['css', 'jekyll-build', 'serve']);
