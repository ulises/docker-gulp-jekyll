FROM ubuntu

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -qq
RUN apt-get install -y vim
RUN apt-get install -y git-core
RUN apt-get install -y curl
RUN apt-get install -y zlib1g-dev
RUN apt-get install -y build-essential
RUN apt-get install -y nodejs
RUN apt-get install -y software-properties-common
RUN apt-add-repository ppa:brightbox/ruby-ng \
  && apt-get update -qq
RUN apt-get -y install ruby2.4 ruby2.4-dev

SHELL ["/bin/bash", "-c"]

RUN gem install jekyll
RUN gem install bundler
RUN gem install minima
RUN gem install jekyll-feed
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - \
  && apt-get install -y nodejs \
  && npm install gulp-cli -g